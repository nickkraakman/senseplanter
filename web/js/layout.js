function toggle_menu()
{
	// Show/hide sidebar menu
	$("#toggle-menu").click(function(){
		console.log("Clicked!");
        $("#sidenav").slideToggle(200);
    });
}

$(document).ready(function() {
	toggle_menu();
});