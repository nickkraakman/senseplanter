<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=garden-yii',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
