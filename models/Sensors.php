<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sensors".
 *
 * @property string $ID
 * @property string $node_id
 * @property string $sensor_title
 * @property string $sensor_description
 * @property string $sensor_type
 * @property string $sensor_format
 * @property integer $sensor_minimum
 * @property integer $sensor_maximum
 * @property string $sensor_key
 */
class Sensors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sensors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['node_id', 'sensor_title', 'sensor_type', 'sensor_key'], 'required'],
            [['node_id'], 'integer'],
            [['sensor_minimum', 'sensor_maximum'], 'number'],
            [['sensor_title', 'sensor_description'], 'string'],
            [['sensor_type', 'sensor_format', 'sensor_key'], 'string', 'max' => 20],
            [['sensor_key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'node_id' => 'Node',
            'sensor_title' => 'Title',
            'sensor_description' => 'Description',
            'sensor_type' => 'Type',
            'sensor_format' => 'Format',
            'sensor_minimum' => 'Minimum',
            'sensor_maximum' => 'Maximum',
            'sensor_key' => 'Key',
        ];
    }
}
