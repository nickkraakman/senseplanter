<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sensor_readings".
 *
 * @property string $ID
 * @property integer $node_id
 * @property string $sensor_key
 * @property string $timestamp
 * @property double $value
 */
class Readings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sensor_readings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['node_id', 'sensor_key', 'value'], 'required'],
            [['node_id'], 'integer'],
            [['timestamp'], 'safe'],
            [['value'], 'number'],
            [['sensor_key'], 'string', 'max' => 20],
            //[['secret'], 'compare', 'compareValue' => '236E16DAD6F65', 'operator' => '=='],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'node_id' => 'Node ID',
            'sensor_key' => 'Sensor Key',
            'timestamp' => 'Timestamp',
            'value' => 'Value',
        ];
    }
}
