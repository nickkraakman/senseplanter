<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Nodes;

/**
 * NodesSearch represents the model behind the search form about `app\models\Nodes`.
 */
class NodesSearch extends Nodes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['node_title', 'node_description', 'node_type', 'node_status', 'node_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Nodes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'node_created' => $this->node_created,
        ]);

        $query->andFilterWhere(['like', 'node_title', $this->node_title])
            ->andFilterWhere(['like', 'node_description', $this->node_description])
            ->andFilterWhere(['like', 'node_type', $this->node_type])
            ->andFilterWhere(['like', 'node_status', $this->node_status]);

        return $dataProvider;
    }
}
