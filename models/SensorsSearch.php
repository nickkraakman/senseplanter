<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sensors;

/**
 * SensorsSearch represents the model behind the search form about `app\models\Sensors`.
 */
class SensorsSearch extends Sensors
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'node_id', 'sensor_minimum', 'sensor_maximum'], 'integer'],
            [['sensor_title', 'sensor_description', 'sensor_type', 'sensor_format', 'sensor_key'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sensors::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'node_id' => $this->node_id,
            'sensor_minimum' => $this->sensor_minimum,
            'sensor_maximum' => $this->sensor_maximum,
        ]);

        $query->andFilterWhere(['like', 'sensor_title', $this->sensor_title])
            ->andFilterWhere(['like', 'sensor_description', $this->sensor_description])
            ->andFilterWhere(['like', 'sensor_type', $this->sensor_type])
            ->andFilterWhere(['like', 'sensor_format', $this->sensor_format])
            ->andFilterWhere(['like', 'sensor_key', $this->sensor_key]);

        return $dataProvider;
    }
}
