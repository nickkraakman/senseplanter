<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nodes".
 *
 * @property string $ID
 * @property string $node_title
 * @property string $node_description
 * @property string $node_type
 * @property string $node_status
 * @property string $node_created
 */
class Nodes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nodes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['node_title'], 'required'],
            [['node_title', 'node_description'], 'string'],
            [['node_created'], 'safe'],
            [['node_type', 'node_status'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'node_title' => 'Title',
            'node_description' => 'Description',
            'node_type' => 'Type',
            'node_status' => 'Status',
            'node_created' => 'Creation Date',
        ];
    }
}
