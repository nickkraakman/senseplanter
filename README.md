SensePlanter
============================

SensePlanter offers a web interface for your connected garden.

You could use an Arduino microcontroller with an ethernet shield, attach some sensors to it, and send
the data to your SensePlanter webserver. You'll be able to view your latest readings, as well as graphs
with historical data. 

In future releases it will also be possible to receive a warning text when a value, like the air temperature,
drops into dangerous territory, or even control the heat, light, and water levels through SensePlanter.


REQUIREMENTS
------------

The minimum requirement of this project is that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Setting up your webserver

Create a webserver and upload all project files.


### Configure database connection

To use SensePlanter, you will first need to configure your database connection. To do this, edit the file 
`config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```


### Run installation file

Once your database has been configured correctly, open the `install/index` page to auto-install some basic
configuration data needed to start using SensePlanter. Do this by opening the following url in your browser:

~~~
http://*YOURSERVER*/web/index.php?r=install
~~~

You are now ready to start using SensePlanter!

To log in, use `test` as both username and password. You can change these login credentials in `models/User.php`.



USAGE
-------------

### Configure nodes and sensors

Start by navigating to the `Nodes` page to create your first node. A node is a system to which you connect your 
sensors, for example an Arduino microcontroller. 

When you have created at least one node, navigate to the `Sensors` page to create sensors and attach them to
your node(s).


### Sending and receiving data

To send sensor data to your SensePlanter webserver, you should construct a url similar to the following:

~~~
http://*YOURSERVER*/web/index.php?r=readings/store&s=*SECRETKEY*&*SENSORKEY*=*SENSORVALUE*
~~~

The `Secret Key` can be found on the `Settings` page, and the `Sensor Key` can be found on the `Sensors` page.

If you are using an Arduino, you can use the example Arduino file `arduino/garden/garden.ino` as a starting point.

