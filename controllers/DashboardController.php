<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Dashboard;
use app\models\Readings;
use app\models\Nodes;
use app\models\Sensors;
use yii\data\ActiveDataProvider;

class DashboardController extends Controller
{
	// Redirect all non-logged in users to login page
	// See: http://code.tutsplus.com/tutorials/how-to-program-with-yii2-user-access-controls--cms-23173
	// See: https://gist.github.com/r3verser/4888b352962dd7de5e3a
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [
                    /*[
                        'actions' => ['login', 'signup'], // those action only which guest (?) user can access
                        'allow' => true,
                        'roles' => ['?'],                        
                    ],*/
                    [
                        'actions' => ['index'],  // those action only which authorized (@) user can access
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

	/**
     * Displays main dashboard page.
     *
     * @return string
     */
    public function actionIndex()
    {	
    	// If identity is NULL, user is NOT authenticated
    	if(Yii::$app->user->identity == NULL) {
    		return $this->goHome();
    		//return $this->redirect(Yii::$app->getUser()->loginUrl);
    	}

    	// TODO: Selected node_id, sensor_id, and time period should be parameters

    	// Find all nodes
		$nodes = Nodes::find()
        			->orderBy('ID')
                    ->all();

        // If at least one node exists, continue
        if(isset($nodes[0])) {
        	// Check if node is selected, else use default
			$get = Yii::$app->request->get(); //retrieve all GET params in the url
			if (isset($get['selected_node'])) {
			    $selected_node = $get['selected_node'];
			} else {
				$selected_node = $nodes[0]->ID;	// Default node ID is the first node in the list
			}
			
			$limit = 1;


			// Find all sensors for certain node ID
	        $sensors = Sensors::find()
	        			->where(['node_id' => $selected_node])
	                    ->all();


	        // Retrieve LATEST data point from the sensor_readings table for this sensor
	        // and store in array
	        // TODO: if latest data point creation date > 1h ago or > average difference ago, show error            
	        $stats_blocks = array(); 
	        foreach($sensors as $key => $value) {

		    	$query = new \yii\db\Query;
			    $query->select('*')
			        	->from('sensors')
			        	->where(['sensors.sensor_key' => $value['sensor_key']])
			            ->leftJoin('sensor_readings', 'sensors.sensor_key = sensor_readings.sensor_key')
			            ->orderBy(['timestamp' => SORT_DESC])
			            ->limit($limit);
			    $command = $query->createCommand();
		    	$latest_data = $command->queryAll();

		    	$stats_blocks[$key] = $latest_data;   
			}


			// Check if sensor is selected, else use default
			if (isset($get['selected_sensor'])) {
			    $selected_sensor = $get['selected_sensor'];
			} elseif ($sensors) {	
				$selected_sensor = $sensors[0]->sensor_key;	// Default sensor key is the first sensor in the list
			} else {
				$selected_sensor = "";
			}	

			// Check if range is selected, else use default
			if (isset($get['range'])) {
			    $range = $get['range'];
			} else {
				$range = 'week';	// Default range
			}

			// Pick query for Google Charts based on selected range
			switch ($range) {
			    case "day":
			        $readings_query = '
		        		SELECT timestamp AS date, value 
						FROM sensor_readings 
						WHERE sensor_key = "'.$selected_sensor.'" AND timestamp >=Subdate(Curdate(), INTERVAL 1 day) AND value != 0
						ORDER BY date
						';
			        break;
			    case "week":
				    $readings_query = '
		        		SELECT DATE_FORMAT(timestamp, "%d-%H") AS date, value 
						FROM sensor_readings 
						WHERE sensor_key = "'.$selected_sensor.'" AND timestamp >=Subdate(Curdate(), INTERVAL 1 week) AND value != 0
						GROUP BY date 
						ORDER BY date
						';
			        break;
			    case "month":
			    	$readings_query = '
		        		SELECT DATE_FORMAT(timestamp, "%d-%H") AS date, value 
						FROM sensor_readings 
						WHERE sensor_key = "'.$selected_sensor.'" AND timestamp >=Subdate(Curdate(), INTERVAL 1 month) AND value != 0
						GROUP BY date 
						ORDER BY date
						';
			        break;
			    case "year":
			    	$readings_query = '
		        		SELECT DATE_FORMAT(timestamp, "%Y-%m-%d") AS date, value 
						FROM sensor_readings 
						WHERE sensor_key = "'.$selected_sensor.'" AND timestamp>=Subdate(Curdate(), INTERVAL 1 year) AND value != 0
						GROUP BY date 
						ORDER BY date
						';
			        break;
			    default:
			    	$readings_query = '
		        		SELECT DATE_FORMAT(timestamp, "%d-%H") AS date, value 
						FROM sensor_readings 
						WHERE sensor_key = "'.$selected_sensor.'" AND timestamp >=Subdate(Curdate(), INTERVAL 1 week) AND value != 0
						GROUP BY date 
						ORDER BY date
						';
			}

			// Retrieve all readings data for the selected sensor, for the selected
			// time period. Default period is Day, others are Week, Month, Year
			// See: http://www.anil2u.info/2011/06/how-to-calculate-statistics-in-daily-weekly-monthly-and-yearly-using-php/
			//$selected_sensor = array("sensor_key" => "humidity", "sensor_title" => "Humidity");

			// Execute query
	    	$readings = Yii::$app->db->createCommand($readings_query)->queryAll();

	        /* Create an array which Google Charts accepts
	        // We need an array like:

	        array(
	            array('Date', 'Value'),
	            array('2004', 1000),
	            array('2005', 1170),
	            array('2006', 660),
	            array('2007', 1030),
	        )
	        which prints like:
	        Array
			(
			    [0] => Array
			        (
			            [0] => Year
			            [1] => Sales
			            [2] => Expenses
			        )

			    [1] => Array
			        (
			            [0] => 2004
			            [1] => 1000
			            [2] => 400
			        )
	        */
		
	        $chart_readings = array(array('Date', 'Value')); // Set column headers

	        foreach ($readings as $key => $value) {
	        	$new_array = array($readings[$key]['date'], (float) $readings[$key]['value']);
	        	array_push($chart_readings, $new_array);
	        }


	    	// Open dashboard index and send along the variables
	        return $this->render('index', [
	        	'nodes' => $nodes,
	        	'stats_blocks' => $stats_blocks,
	        	'selected_node' => $selected_node,
	        	'chart_readings' => $chart_readings,
	        	'selected_sensor' => $selected_sensor,
	        	'range' => $range,
	        ]);
        } else {
        	// Redirect to create node page and show info message
	        return $this->redirect(['nodes/create', 'reason' => 1]);
        }
    }
}