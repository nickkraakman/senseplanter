<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\base\Model;
use app\models\Settings;

// SEE: http://www.yiiframework.com/doc-2.0/guide-input-tabular-input.html
// SEE: http://www.yiiframework.com/wiki/805/simple-way-to-implement-dynamic-tabular-inputs/
class SettingsController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],  // those action only which authorized (@) user can access
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        // Load all Settings models
        $settings = Settings::find()->indexBy('setting_name')->all();

        if (Model::loadMultiple($settings, Yii::$app->request->post()) && Model::validateMultiple($settings)) {
            foreach ($settings as $setting) {
                $setting->save(false);
            }
            //return $this->redirect('index');
            return $this->render('index', [
                'settings' => $settings,
                'message' => 'Saved!',
            ]);
        }

        return $this->render('index', ['settings' => $settings]);
    }

    public function actionUpdate()
    {
        return $this->render('update');		
    }

    public function actionCreate()
    {
        //Find out how many settings have been submitted by the form
        $count = count(Yii::$app->request->post('Settings', []));
        echo $count;

        //Send at least one model to the form
        $settings = [new Settings()]; // add more to add extra form fields
        
        if (Model::loadMultiple($settings, Yii::$app->request->post()) && Model::validateMultiple($settings)) {
            
            //Try to save the models. Validation is not needed as it's already been done.
            foreach ($settings as $setting) {
                $setting->save(false);
            }
            //return $this->redirect(['index', 'message' => 'Saved!']);
            return $this->render('create', [
                'settings' => $settings,
                'message' => 'Saved!',
            ]);
        } else {
            return $this->render('create', [
                'settings' => $settings,
            ]);
        }
    }
}
