<?php

namespace app\controllers;

use Yii;

class InstallController extends \yii\web\Controller
{
    public function actionIndex()
    {
    	// Insert example data into database
    	// TODO: handle if no data has been inserted yet or has already been inserted, also on other pages
    	$settings = array(
    		'email'			=> 'example@example.com',
    		'secret'		=> Yii::$app->getSecurity()->generateRandomString(6),
    		'name'			=> 'John Smith',
    		'site_title'	=> 'My Garden',
    	); 

    	$errors = 0;

		try {
			foreach($settings as $key => $value) {
				$command = Yii::$app->db->createCommand()->insert('settings', [
				    'setting_name' => $key,
				    'setting_value' => $value,
				])->execute();
			}
		} catch (\yii\db\Exception $e) {
		   	 $errors += 1;
		} 

        return $this->render('index', [
            'errors' => $errors,
        ]);
    }

}
