<?php

namespace app\controllers;

use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use app\models\Readings;
use app\models\ReadingsSearch;
use app\models\Settings;
use app\models\Sensors;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReadingsController implements the CRUD actions for Readings model.
 */
class ReadingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [
                    [
                        'actions' => ['store'], // those action only which guest (?) user can access
                        'allow' => true,
                        'roles' => ['?'],                        
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'store'],  // those action only which authorized (@) user can access
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
   
    /**
     * Lists all Readings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReadingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = ['defaultOrder' => ['timestamp' => 'DESC']]; // Show latest reading first

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Readings model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Readings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Readings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Store a new sensor reading in the database.
     * Keep parameters short to save SRAM memory on Arduino
     * Add sensor_key=value in the URL
     * Example request URL: /garden-yii/web/index.php?r=readings%2Fstore&s=236E16&HpSA4d=23.54&MBvvWY=64.07
     * @see http://www.tutorialspoint.com/yii/yii_http_requests.htm Tutorial on Yii HTTP requests
     */
    public function actionStore()
    {
        // Store all POST/GET data in variable
        //$data = Yii::$app->request->post();
        $data = Yii::$app->request->get();

        // Load secret from settings table
        $setting = Settings::findOne(["setting_name" => 'secret']);

        // Check if the correct secret is set
        $secret = $setting->setting_value;
        if(!isset($data["s"]) || $data["s"] != $secret) {   // s is secret
            Yii::error("Secret is not set or incorrect");
            echo "Secret is not set or incorrect <br />";
            return;
        }

        // Walk through all the posted data
        foreach($data as $key => $value) {
            // Skip secret key and r parameters to only get the data points
            if($key != "s" && $key != "r") {
                // Check if the key exists and to which node it belongs
                if($sensor = Sensors::findOne(["sensor_key" => $key])){

                    $model = new Readings();

                    $model->node_id = $sensor['node_id'];
                    $model->sensor_key = $key;
                    $model->value = $value;

                    // Check the model for errors
                    if($model->validate())
                    {
                        echo 'No errors in the model!<br />';
                    }
                    else
                    {
                        echo "Errors found in the model!<br />";
                        $errors = $model->getErrors();
                        echo "<pre>";
                        print_r($errors);
                        echo "</pre>";
                        Yii::error($errors);
                    }

                    // Save the model in the database
                    if ($model->save()) {
                        echo "Saved!<br />";
                    }
                } else {
                    echo "No sensor with key ".$key." found!";
                    Yii::warning("No sensor with key ".$key." found!");         
                }
            }
        }

        return;
    }

    /**
     * Updates an existing Readings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Readings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Readings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Readings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Readings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
