<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Nodes;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteControllerMine extends Controller
{   

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

	/**
     * Displays index page.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect('dashboard/index', 302);
    }

    /**
     * Displays dashboard page.
     *
     * @return string
     */
    public function actionDashboard()
    {
        return $this->render('dashboard');
    }

    /**
     * Displays nodes page.
     *
     * @return string
     */
    public function actionNodes()
    {
        /* A dataprovider with all nodes from database */
        $dataProvider = new ActiveDataProvider([
            'query' => Nodes::find(),
        ]);
        return $this->render('nodes', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays settings page.
     *
     * @return string
     */
    public function actionSettings()
    {
        return $this->render('settings');
    }
}