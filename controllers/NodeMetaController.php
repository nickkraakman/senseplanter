<?php

namespace app\controllers;

class NodeMetaController extends \yii\web\Controller
{
	/**
     * Add a new sensor to a node.
     * If adding is successful, the create page will reload.
     * @return mixed
     */
    public function actionAdd()
    {
        // If identity is NULL, user is NOT authenticated
        if(Yii::$app->user->identity == NULL) {
            return $this->goHome();
            //return $this->redirect(Yii::$app->getUser()->loginUrl);
        }
        
        $modelSensor = new NodeMeta();

        if ($modelSensor->load(Yii::$app->request->post())) {
            if ($modelSensor->validate()) {
                // form inputs are valid, do something here
                return;
            }
        }

        return $this->render('add', [
        //return $this->render('create', [
            'modelSensor' => $modelSensor,
        ]);
    }

}
