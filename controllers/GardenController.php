<?php

namespace app\controllers;

use yii\web\Controller;

class GardenController extends Controller
{

	/**
     * Displays index page.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays dashboard page.
     *
     * @return string
     */
    public function actionDashboard()
    {
        return $this->render('dashboard');
    }

    /**
     * Displays nodes page.
     *
     * @return string
     */
    public function actionNodes()
    {
        return $this->render('nodes');
    }

    /**
     * Displays settings page.
     *
     * @return string
     */
    public function actionSettings()
    {
        return $this->render('settings');
    }
}