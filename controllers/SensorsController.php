<?php

namespace app\controllers;

use Yii;
use app\models\Sensors;
use app\models\SensorsSearch;
use app\models\Nodes;
use app\models\Readings;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * SensorsController implements the CRUD actions for Sensors model.
 */
class SensorsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),                
                'rules' => [
                    /*[
                        'actions' => ['login', 'signup'], // those action only which guest (?) user can access
                        'allow' => true,
                        'roles' => ['?'],                        
                    ],*/
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],  // those action only which authorized (@) user can access
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sensors models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Find all nodes
        $nodes = Nodes::find();

        //$nodes = $query->orderBy('ID')
        //    ->all();

        // View sensors in table
        $searchModel = new SensorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'nodes' => $nodes,
        ]);
    }

    /**
     * Displays a single Sensors model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sensors model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // Find all nodes
        $query = Nodes::find();

        $nodes = $query->orderBy('ID')
                    ->all();

        // If at least one node exists, continue
        if(isset($nodes[0])) {
            $model = new Sensors();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->ID]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'nodes' => $nodes,
                ]);
            }
        } else {
            return $this->redirect(['nodes/create', 'reason' => 1]);
        }
    }

    /**
     * Updates an existing Sensors model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Find all nodes
        $query = Nodes::find();

        $nodes = $query->orderBy('ID')
                    ->all();
        
        $model = $this->findModel($id);

        // Store the current sensor key in a variable
        $old_sensor_key = $model->sensor_key;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // Store the new sensor key in a variable
            $new_sensor_key = $model->sensor_key;

            // On update of the sensor_key, update all readings with that sensor_key to the new sensor_key
            $command = Yii::$app->db->createCommand();
            $command->update('sensor_readings', array(
                'sensor_key' => $new_sensor_key,
            ), 'sensor_key=:sensor_key', array(':sensor_key' => $old_sensor_key));
            $command->execute();

            $model->save();
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'nodes' => $nodes,
            ]);
        }
    }

    /**
     * Deletes an existing Sensors model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sensors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Sensors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sensors::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Generates a string of $length, then checks the database to see
     * if string is unique, and if not to generate a new string.
     * @param string $attribute In our case the sensor_key
     * @param int $length Length of the returned string
     * @return string Unique random string
     * @see http://www.jamesbarnsley.com/site/2016/04/25/generating-a-unique-random-string-for-model-properties-in-yii2/
     */
    public function uniqueString($attribute, $length = 32) {
            
        $randomString = Yii::$app->getSecurity()->generateRandomString($length);
                
        if(!Sensors::findOne([$attribute => $randomString])){
            return $randomString;
        } else {
            return $this->uniqueString($attribute, $length);
        }    
    }
}
