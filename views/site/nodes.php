<?php
// Nodes View
// Access through: http://localhost:8888/garden-yii/web/index.php?r=garden/nodes

use yii\helpers\Html;
use kartik\grid\GridView; // More advanced GridView module
// use yii\grid\GridView;
// use yii\widgets\LinkPager; // Later to add pagination

// See: http://demos.krajee.com/grid-demo?BookSearch%5Bname%5D=&booksearch-color-source=black&BookSearch%5Bcolor%5D=&BookSearch%5Bauthor_id%5D=&BookSearch%5Bpublish_date%5D=&BookSearch%5Bbuy_amount%5D=&BookSearch%5Bsell_amount%5D=&BookSearch%5Bstatus%5D=&_pjax=%23kv-grid-demo-pjax
// See: http://demos.krajee.com/grid
// See: http://webtips.krajee.com/setup-editable-column-grid-view-manipulate-records/

$this->title = 'Nodes | Arduino Garden';
?>

<div class="site-index">
    <h1 class="view-title">Nodes</h1>
</div>

<?php
// Render grid of nodes from database
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'hover'=>true,
    'bordered'=>false,
    'responsive'=>true,
    'columns' => [
        'ID',
        'NodeTitle',
        'CreationDate',
        [
	        'class' => '\kartik\grid\ActionColumn',
	        'deleteOptions' => ['label' => '<i class="glyphicon glyphicon-remove"></i>']
	    ],
    ],
    'pjax'=>true, // pjax is set to always true for this demo
    'panel'=>[
        'type'=>GridView::TYPE_DEFAULT,
        'heading'=>false,
    ],
    // set your toolbar
    'toolbar'=> [
        ['content'=>
            Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>'Add Node', 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=>'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    // set export properties
    'export'=>[
        'fontAwesome'=>true
    ],
]);
?>