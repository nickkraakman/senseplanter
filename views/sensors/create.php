<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Sensors */

$this->title = 'Create Sensor';
$this->params['breadcrumbs'][] = ['label' => 'Sensors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sensors-create">
	<div class="row row-header">
        <div class="col-md-12">
            <h1 class="view-title"></span> <?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
		    <?= $this->render('_form', [
		        'model' => $model,
		        'nodes' => $nodes,
		    ]) ?>
		</div>
	</div>
</div>
