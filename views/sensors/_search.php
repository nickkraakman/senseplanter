<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SensorsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sensors-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'node_id') ?>

    <?= $form->field($model, 'sensor_title') ?>

    <?= $form->field($model, 'sensor_description') ?>

    <?= $form->field($model, 'sensor_type') ?>

    <?php // echo $form->field($model, 'sensor_format') ?>

    <?php // echo $form->field($model, 'sensor_minimum') ?>

    <?php // echo $form->field($model, 'sensor_maximum') ?>

    <?php // echo $form->field($model, 'sensor_key') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
