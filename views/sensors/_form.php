<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sensors */
/* @var $form yii\widgets\ActiveForm */

// Store all node IDs and titles in array
$node_ids = array();
foreach($nodes as $node) {
    $node_id = Html::encode("{$node->ID}");
    $node_title = Html::encode("{$node->node_title}");
    $node_ids[$node_id] = $node_title." ($node_id)";    // Add to associative array
}

// Options for the sensor_types dropdown
$sensor_types = array(
    'water_temp' => 'Water temperature', 
    'soil_temp' => 'Soil temperature',
    'soil_moisture' => 'Soil moisture', 
    'air_temp' => 'Air temperature', 
    'air_humidity' => 'Humidity',
    'ph' => 'pH value',
    'ec' => 'Conductivity',
    'light' => 'Light',
);

// Options for the sensor_format dropdown
$sensor_formats = array(
    'percentage' => 'Percentage', 
    'number' => 'Number', 
    'float' => 'Decimal', 
    'degrees' => 'Degrees'
);

?>

<div class="sensors-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'node_id')->dropDownList($node_ids) ?>

    <?= $form->field($model, 'sensor_title')->textInput() ?>

    <?= $form->field($model, 'sensor_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sensor_type')->dropDownList($sensor_types) ?>

    <?= $form->field($model, 'sensor_format')->dropDownList($sensor_formats) ?>

    <?= $form->field($model, 'sensor_minimum')->textInput() ?>

    <?= $form->field($model, 'sensor_maximum')->textInput() ?>

    <?= $form->field($model, 'sensor_key')->textInput([
        'maxlength' => true, 
        'readonly' => true,
        $model->sensor_key != '' ?  : 'value' => $this->context->uniqueString('sensor_key', 6), // Generate 6 character unique key for this sensor if none is set
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-warning' : 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
