<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sensors */

$this->title = "Sensor ".$model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Sensors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sensors-view">
    <div class="row row-header">
        <div class="col-md-8">
            <h1 class="view-title"></span> <?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-warning']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'ID',
                    'node_id',
                    'sensor_title:ntext',
                    'sensor_description:ntext',
                    'sensor_type',
                    'sensor_format',
                    'sensor_minimum',
                    'sensor_maximum',
                    'sensor_key',
                ],
            ]) ?>
        </div>
    </div>
</div>
