<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SensorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sensors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sensors-index">
    <div class="row row-header">
        <div class="col-md-8">
            <h1 class="view-title"><span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span> <?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
        <div class="col-md-4">
            <div>
                <?= Html::a('Create Sensor', ['create'], ['class' => 'btn btn-warning pull-right']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php Pjax::begin(); ?>    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'ID',
                        'node_id',
                        'sensor_title:ntext',
                        'sensor_description:ntext',
                        'sensor_type',
                        'sensor_format',
                        'sensor_minimum',
                        'sensor_maximum',
                        'sensor_key',

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
