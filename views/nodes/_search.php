<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NodesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nodes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'node_title') ?>

    <?= $form->field($model, 'node_description') ?>

    <?= $form->field($model, 'node_type') ?>

    <?= $form->field($model, 'node_status') ?>

    <?php // echo $form->field($model, 'node_created') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
