<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Nodes */
/* @var $form yii\widgets\ActiveForm */

//$form->field($model, 'node_type')->textInput(['maxlength' => true])
//$form->field($model, 'node_status')->textInput(['maxlength' => true])
//$form->field($model, 'node_created')->textInput()
?>

<div class="nodes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'node_title')->textInput() ?>

    <?= $form->field($model, 'node_description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-warning' : 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
