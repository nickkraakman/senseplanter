<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView; // More advanced GridView module
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NodesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nodes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nodes-index">
    <div class="row row-header">
        <div class="col-md-8">
            <h1 class="view-title"><span class="glyphicon glyphicon-record" aria-hidden="true"></span> <?= Html::encode($this->title) ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
        <div class="col-md-4">
            <div>
                <?= Html::a('Create Node', ['create'], ['class' => 'btn btn-warning pull-right']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                //'hover'=>true,
                //'bordered'=>false,
                //'responsive'=>true,
                'pjax'=>true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'ID',
                    'node_title:ntext',
                    'node_description:ntext',
                    'node_type',
                    'node_status',
                    'node_created',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
        </div>
    </div>
</div>
