<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Readings */

$this->title = 'Update Reading: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Readings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="readings-update">
	<div class="row row-header">
        <div class="col-md-12">
            <h1 class="view-title"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-12">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
