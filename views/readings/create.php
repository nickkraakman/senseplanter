<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Readings */

$this->title = 'Create Reading';
$this->params['breadcrumbs'][] = ['label' => 'Readings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="readings-create">
	<div class="row row-header">
        <div class="col-md-12">
            <h1 class="view-title"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <div class="row">
    	<div class="col-md-12">
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
