<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Readings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="readings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'node_id')->textInput() ?>

    <?= $form->field($model, 'sensor_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-warning' : 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
