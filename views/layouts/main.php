<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\sidenav\SideNav;
use yii\helpers\Url;
use app\models\Settings;
use yii\bootstrap\Collapse;

AppAsset::register($this);

// Get site_title value from settings table in database
$site_title = Settings::findOne(['setting_name' => 'site_title']);

// Load layout.js for this view, after jQuery
$this->registerJsFile('@web/js/layout.js', ['depends' => '\yii\web\JqueryAsset']);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="col-md-2 no-padding" id="sidebar">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= $site_title->setting_value; ?></h3>
                <div id="toggle-menu">
                    <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                </div>
            </div>
        </div>

        <?php

        // Sidenav
        $item = 'home';
        echo SideNav::widget([
            'type' => 'default',
            'encodeLabels' => false,
            //'heading' => $site_title->setting_value,
            'containerOptions' => ['id' => 'sidenav'],
            'items' => [
                // Important: you need to specify url as 'controller/action',
                // not just as 'controller' even if default action is used.
                /*['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => Url::to(['/dashboard/index', 'type'=>$type]), 'active' => ($item == 'dashboard')],
                ['label' => 'Books', 'icon' => 'book', 'items' => [
                    ['label' => '<span class="pull-right badge">10</span> New Arrivals', 'url' => Url::to(['/site/new-arrivals', 'type'=>$type]), 'active' => ($item == 'new-arrivals')],
                    ['label' => '<span class="pull-right badge">5</span> Most Popular', 'url' => Url::to(['/site/most-popular', 'type'=>$type]), 'active' => ($item == 'most-popular')],
                    ['label' => 'Read Online', 'icon' => 'cloud', 'items' => [
                        ['label' => 'Online 1', 'url' => Url::to(['/site/online-1', 'type'=>$type]), 'active' => ($item == 'online-1')],
                        ['label' => 'Online 2', 'url' => Url::to(['/site/online-2', 'type'=>$type]), 'active' => ($item == 'online-2')]
                    ]],
                ]],
                ['label' => '<span class="pull-right badge">3</span> Categories', 'icon' => 'tags', 'items' => [
                    ['label' => 'Fiction', 'url' => Url::to(['/site/fiction', 'type'=>$type]), 'active' => ($item == 'fiction')],
                    ['label' => 'Historical', 'url' => Url::to(['/site/historical', 'type'=>$type]), 'active' => ($item == 'historical')],
                    ['label' => '<span class="pull-right badge">2</span> Announcements', 'icon' => 'bullhorn', 'items' => [
                        ['label' => 'Event 1', 'url' => Url::to(['/site/event-1', 'type'=>$type]), 'active' => ($item == 'event-1')],
                        ['label' => 'Event 2', 'url' => Url::to(['/site/event-2', 'type'=>$type]), 'active' => ($item == 'event-2')]
                    ]],
                ]],*/
                ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => Url::to(['/dashboard/index']), 'active' => ($item == 'dashboard')],
                ['label' => 'Nodes', 'icon' => 'record', 'url' => Url::to(['/nodes/index']), 'active' => ($item == 'nodes')],
                ['label' => 'Sensors', 'icon' => 'cloud-upload', 'url' => Url::to(['/sensors/index']), 'active' => ($item == 'sensors')],
                ['label' => 'Readings', 'icon' => 'search', 'url' => Url::to(['/readings/index',]), 'active' => ($item == 'readings')],
                //['label' => 'Triggers', 'icon' => 'off', 'url' => Url::to(['/triggers/index']), 'active' => ($item == 'triggers')],
                ['label' => 'Settings', 'icon' => 'cog', 'url' => Url::to(['/settings/index']), 'active' => ($item == 'settings')],
                ['label' => 'Log out', 'icon' => 'user', 'url' => Url::to(['/site/logout']), 'active' => ($item == 'user'), 'options' => ['class' => 'border-top']],
            ],
        ]);     
        ?>

        <?php
        /*echo Collapse::widget([
            'encodeLabels' => false,
            'items' => [
                // equivalent to the above
                [
                    'label' => 'Click me!',
                    'content' => $sidenav.$sidenav_user,
                    // open its content by default
                    'contentOptions' => ['class' => 'in']
                ]
            ]
        ]);   */
        ?>

        
    </div>
    <div class="col-md-10" id="content">
        <!-- echo the main content of the page, the view -->
        <?= $content ?>
    </div>
</div>

<!--
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Arduino Garden <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
