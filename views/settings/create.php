<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\web\JqueryAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form ActiveForm */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;

// Load settings.js for this view, after jQuery
$this->registerJsFile('@web/js/settings.js', ['depends' => '\yii\web\JqueryAsset']);

// See: http://stackoverflow.com/questions/32481399/yii2-insert-multiple-records-of-a-same-table
?>
<div class="nodes-index">
    <div class="row row-header">
        <div class="col-md-8">
            <h1 class="view-title"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-md-4">
            <div>
                <?= Html::submitButton('Save', ['class' => 'btn btn-warning pull-right', 'id' => 'save-form']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

        	<?php 

        	$form = ActiveForm::begin();

        	echo $form->field($settings[0], "[0]setting_name")->hiddenInput(['value' => 'email'])->label(false);
        	echo $form->field($settings[0], "[0]setting_value")->label('Email');

        	echo $form->field($settings[0], "[1]setting_name")->hiddenInput(['value' => 'name'])->label(false);
        	echo $form->field($settings[0], "[1]setting_value")->label('Name');

        	/*foreach ($settings as $index => $setting) {
			    echo $form->field($setting, "[$index]setting_name")->label($setting->setting_name);
			    echo $form->field($setting, "[$index]setting_value")->label($setting->setting_value);
			}*/

        	ActiveForm::end();
        	?>

        </div>
    </div>
</div>