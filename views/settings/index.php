<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\web\JqueryAsset;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form ActiveForm */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;

// Load settings.js for this view, after jQuery
$this->registerJsFile('@web/js/settings.js', ['depends' => '\yii\web\JqueryAsset']);

// See: http://stackoverflow.com/questions/32481399/yii2-insert-multiple-records-of-a-same-table
?>
<div class="nodes-index">
    <div class="row row-header">
        <div class="col-md-8">
            <h1 class="view-title"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-md-4">
            <div>
                <?= Html::submitButton('Save', ['class' => 'btn btn-warning pull-right', 'id' => 'save-form']) ?>
            </div>
        </div>
    </div>
    <?php
    // Show message on successful save
    if(isset($message) && $message != "") {
        echo Alert::widget([
            'options' => [
                'class' => 'alert-success',
            ],
            'body' => 'Settings saved!',
        ]);
    } ?>
    <div class="row">
        <div class="col-md-3">
            <div class="side-titles">
                <h2>General</h2>
                <p class="subtitle">Basic information.</p>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
            	<?php 

            	$form = ActiveForm::begin();
                
            	// The number behind $settings is the setting_id from the settings table
                echo $form->field($settings['site_title'], "[site_title]setting_value")->label('Site Title');
            	echo $form->field($settings['name'], "[name]setting_value")->label('First Name');
                echo $form->field($settings['email'], "[email]setting_value")->label('Email');
                echo $form->field($settings['secret'], "[secret]setting_value")->label('Secret Key')->textInput(['readonly' => true]);

                /*
                Output all settings fields
            	foreach ($settings as $index => $setting) {
                    echo $form->field($setting, "[$index]setting_value")->label($setting->setting_value);
                }*/

            	ActiveForm::end();
            	?>
            </div>
        </div>
    </div>
</div>