<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\models\Nodes */

$this->title = 'Installation';
$this->params['breadcrumbs'][] = ['label' => 'Installation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="installation-create">
	<div class="row row-header">
        <div class="col-md-12">
            <h1 class="view-title"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
            <?php
            if(isset($errors) && $errors != 0) {
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-warning',
                    ],
                    'body' => '<strong>Whoops!</strong> There were some errors during installation. You might have run the installation before.',
                ]);
            }
            ?>
		    <p>Installation completed!</p>
		</div>
	</div>
</div>