<?php

use yii\helpers\Html;
use yii\helpers\Url;
use scotthuangzl\googlechart\GoogleChart;
use yii\bootstrap\Dropdown;
use yii\bootstrap\ButtonDropdown;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
// Dashboard View
// Access through: http://localhost:8888/garden-yii/web/index.php?r=garden/dashboard

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;

// Set outputs based on sensor format
$format_signs = array(
    'percentage' => '&#37;', // % sign
    'number' => '', 
    'float' => '', 
    'degrees' => '&deg;'	// º sign
);

// Set icon based on sensor type
$icon_urls = array(
	'water_temp' => 'images/icons/water_temperature.png', 
    'soil_temp' => 'images/icons/soil_temperature.png',
    'soil_moisture' => 'images/icons/soil_moisture.png', 
    'air_temp' => 'images/icons/air_temperature.png', 
    'air_humidity' => 'images/icons/air_humidity.png',
    'ph' => 'images/icons/ph_value.png',
    'ec' => 'images/icons/electrical_conductivity.png',
    'light' => 'images/icons/light.png',
);

// Store all node IDs and titles in array
$node_ids = array();
foreach($nodes as $node) {
    $node_id = Html::encode("{$node->ID}");
    $node_title = Html::encode("{$node->node_title}");
    $node_ids[$node_id] = array('label' => $node_title." ($node_id)", 'url' => ['index', 'selected_node' => $node_id]);
}

?>
<div class="site-index">
    <div class="row row-header">
        <div class="col-md-9">
            <h1 class="view-title"><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span> Dashboard</h1>
        </div>
        <div class="col-md-3">
            <div class="select-node-form">
                <?php 
                // See: http://www.yiiframework.com/doc-2.0/yii-bootstrap-buttondropdown.html
                // See: http://getbootstrap.com/components/#dropdowns
                echo ButtonDropdown::widget([
                    'label' => $node_ids[$selected_node]['label'],
                    'dropdown' => [
                        'items' => $node_ids,
                        'options' => [
                            'class' => 'dropdown-menu-right',
                        ],
                    ],
                    'options' => [
                        'class' => 'btn btn-default',
                    ],
                ]); ?>
            </div>
        </div>
    </div>

    <?php 
    // Show warning message if this node does not have a sensor attached
    if(!isset($selected_sensor) || $selected_sensor == "") {
        echo Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => '<strong>Whoops!</strong> You should probably '.Html::a('add some sensors', ['sensors/index',]).' to your node.',
        ]);
    } else {
        // Output stat blocks
        foreach($stats_blocks as $key => $value): 

        // Store url in variable
        $href = Url::toRoute(['index', 'selected_node' => $selected_node, 'range' => $range, 'selected_sensor' => $stats_blocks[$key][0]['sensor_key']]);

        // Check value against min/max to show either green, orange or red icon
        $color_code = "orange";
        if ($stats_blocks[$key][0]['value'] < $stats_blocks[$key][0]['sensor_minimum'] || $stats_blocks[$key][0]['value'] > $stats_blocks[$key][0]['sensor_maximum']) {
            $color_code = "red";
        } else {
            $color_code = "green";
        }
        
        // Every fifth sensor, start a new row
        if($key == 0) {
            echo '<div class="row">';
        } elseif ($key % 4 == 0) {
            echo '</div><div class="row">';
        }

        ?>
        
            <div class="col-md-3">
                <a href="<?php echo $href ?>" class="stats-block-link<?php echo ($selected_sensor == $stats_blocks[$key][0]['sensor_key'] ? "-selected" : "") ?>">
                    <div class="stats-block">
                        <div class="stats-block-left stats-block-<?php echo $color_code; ?>">
                            <img src="<?php echo $icon_urls[$stats_blocks[$key][0]['sensor_type']]; ?>" alt="<?php echo $stats_blocks[$key][0]['sensor_title']; ?>" class="stats-block-img" /> <!-- render icons here -->
                        </div>
                        <div class="stats-block-right">
                            <p class="stats-block-value">
                                <?php 
                                // Check if there is any readings data available for this node/sensor combination
                                if ($stats_blocks[$key][0]['value'] != "") {
                                    echo $stats_blocks[$key][0]['value'].$format_signs[$stats_blocks[$key][0]['sensor_format']]; 
                                } else {
                                    echo "No data";
                                } ?>
                            </p>
                            <p class="stats-block-title"><?php echo $stats_blocks[$key][0]['sensor_title']; ?></p>
                        </div>
                    </div>
                </a>
                <?php
                // Check if data point is more than X minutes old, else show a warning
                $format = ' minutes';
                $minutes = 10;
                $seconds = $minutes * 60;
                $timestamp = strtotime($stats_blocks[$key][0]['timestamp']);
                $now = time();
                if($timestamp < $now - $seconds && $timestamp != null) {
                    $difference = ($now - $timestamp)/60;
                    if($difference > 60) {
                        $difference = $difference/60;   // Minutes to hours
                        $format = ' hours';
                    }
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-warning',
                        ],
                        'body' => 'Data is '.round($difference).$format.' old',
                    ]);
                }
                ?>
            </div>
        <?php endforeach; ?>
        </div>

        <!-- Google Chart -->
        <div class="row">
            <div class="col-md-9">
                <p>
                    <?= Html::a('Day', ['index', 'selected_node' => $selected_node, 'range' => 'day', 'selected_sensor' => $selected_sensor], ['class' => ($range == 'day' ? 'btn btn-default active' : 'btn btn-default')]) ?>
                    <?= Html::a('Week', ['index', 'selected_node' => $selected_node, 'range' => 'week', 'selected_sensor' => $selected_sensor], ['class' => ($range == 'week' ? 'btn btn-default active' : 'btn btn-default')]) ?>
                    <?= Html::a('Month', ['index', 'selected_node' => $selected_node, 'range' => 'month', 'selected_sensor' => $selected_sensor], ['class' => ($range == 'month' ? 'btn btn-default active' : 'btn btn-default')]) ?>
                    <?= Html::a('Year', ['index', 'selected_node' => $selected_node, 'range' => 'year', 'selected_sensor' => $selected_sensor], ['class' => ($range == 'year' ? 'btn btn-default active' : 'btn btn-default')]) ?>
                </p>
                <?php
                // If there are no data points...
                if(!isset($chart_readings[1])) {
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-warning',
                        ],
                        'body' => '<strong>Whoops!</strong> There is no data available for the selected period.',
                    ]);
                } else { 
                    // Display data for selected node/sensor/period combination in chart
                    echo "<div class='card card-chart'>";
                    echo GoogleChart::widget(array('visualization' => 'AreaChart',
                        'data' => $chart_readings,
                        'options' => array(
                            'title' => $selected_sensor,
                            'vAxis' => array(
                                //'minValue' => 0,  // Start vertical axis on 0 = less detail
                                'gridlines' => array(
                                    'color' => 'transparent'  //set grid line transparent
                                )),
                            'curveType' => 'function', //smooth curve or not
                            'legend' => array('position' => 'none'),
                            'height' => 300,
                        )));
                    echo "</div>";
                } 
                ?>
            </div>
        </div>
    <?php
    }
    ?>
</div>