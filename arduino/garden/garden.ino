#include <DHT.h>
#include <Ethernet.h>
#include <SPI.h>

byte mac[] = { 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02 };
byte my_ip[] = { 192, 168, 1, 43 }; // YOUR LOCAL IP ADDRESS
//byte server[] = { 93, 94, 226, 200 };
char server[] = "www.webdesign4me.nl"; // INSTEAD OF IP
String secret = "236E16"; // SECRET KEY
EthernetClient client;

#define DHTPIN 2 // SENSOR PIN
#define DHTTYPE DHT22 // SENSOR TYPE - THE ADAFRUIT LIBRARY OFFERS SUPPORT FOR MORE MODELS
DHT dht(DHTPIN, DHTTYPE);

float temp, hum;
String data;

void setup() { 
  Serial.begin(9600);
 
  Serial.println("... Initializing ethernet");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    // try to configure using IP address instead of DHCP:
    Ethernet.begin(mac, my_ip); 
  }
  Serial.println("... Done initializing ethernet");
  delay(1000);

  dht.begin(); 
  delay(2000); // GIVE THE SENSOR SOME TIME TO START
}

void loop(){
  hum = (float) dht.readHumidity();
  temp = (float) dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(hum) || isnan(temp)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  // Convert temp and hum FLOAT to STRING
  // TODO: Best to convert to Char instead of String, uses way less memory, e.g.
  // char strTemp[5];
  // dtostrf(temp, 5, 2, strTemp);
  String strTemp = String(temp);
  String strHum = String(hum);

  Serial.print("Humidity: "); 
  Serial.print(strHum);
  Serial.print(" %\t");
  Serial.print("Temperature: "); 
  Serial.print(strTemp);
  Serial.println(" *C ");
  
  data = "r=readings%2Fstore&s="+secret+"&HpSA4d="+strTemp+"&MBvvWY="+strHum;
  
  if (client.connect(server,80)) { // REPLACE WITH YOUR SERVER ADDRESS
    Serial.println("Connected to website");
    client.println("GET /garden-yii/web/index.php?"+data+" HTTP/1.1");
    client.println("Host: www.webdesign4me.nl"); // SERVER ADDRESS HERE TOO
    client.println("Content-Type: application/x-www-form-urlencoded; charset=UTF-8");
    client.print("Content-Length: "); 
    client.println(data.length());
    client.println(); 
    client.print(data); 
  } 
  else {
    // You didn't get a connection to the server:
    Serial.println("connection failed");
  }

  if (client.connected()) { 
    client.println("Connection: close"); 
    client.stop();  // DISCONNECT FROM THE SERVER
  }

  delay(60000); // WAIT ONE MINUTE BEFORE SENDING AGAIN
}
